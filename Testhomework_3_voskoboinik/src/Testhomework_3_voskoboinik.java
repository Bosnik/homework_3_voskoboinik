import com.sun.javaws.Main;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Testhomework_3_voskoboinik {


    @Test
    public void TestMinOfThree(){

        assertEquals(3, homework_3_voskoboinik.minOfThree(3, 5, 8));
}



    @Test
    public void TestFactorial(){
        assertEquals(10,homework_3_voskoboinik.factorial(5));
    }

    @Test
    public void TestCompareString(){
        String s = new String("5");
        String s2 = new String("5");
        assertTrue(homework_3_voskoboinik.compareString(s,s2));
    }

    @Test
    public void TestConcatenateString(){
        assertEquals("--hello!!", homework_3_voskoboinik.concatenateString("--","hello","!!"));
    }

    @Test
    public void TestFindMinInArray(){
        int arr[] = {47,54,6,93,83,4,20};
        assertEquals(4,homework_3_voskoboinik.findMinInArray(arr));
    }

    @Test
    public void TestSortArray() {
        int arr[] = {47, 5, 6, 9, 83, 4, 20};
        int result[] = {4, 5, 6, 9, 20, 47, 83};
        assertArrayEquals(result, homework_3_voskoboinik.sortArray(arr));
    }

    @Test
    public void TestSumOfElementsUnderMainDiagonal(){
        int[][] matrix = {
                {10,20,30,40},
                {10,20,30,40},
                {10,20,30,40},
                {10,20,30,40}};
        int result=100;
        assertEquals(result,homework_3_voskoboinik.sumOfElementsUnderMainDiagonal(matrix));
    }

    @Test
    public void TestCountPowerOfTwo(){
        int[][] matrix = {
                {10,2,30,40},
                {10,20,30,40},
                {10,20,30,4},
                {10,20,30,8}};
        assertEquals(3,homework_3_voskoboinik.countPowerOfTwo(matrix));
    }

    @Test(timeout = 1000)
    public void TestSleepFor1000() throws InterruptedException {
        homework_3_voskoboinik.sleepFor1000(950);
    }

    @Test
    public void TestThrowException() throws Exception {
        homework_3_voskoboinik.throwException();
    }
}
